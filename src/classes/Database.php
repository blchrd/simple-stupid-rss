<?php

class Database {
    public static function migrate($databasePath, $migrationDir) {
        if (!file_exists($databasePath)) {
            $database = new PDO("sqlite:$databasePath");
            $dbVersion = 0;
        } else {
            try {
                $database = new PDO("sqlite:$databasePath");
                $stmt = $database->prepare('select * from settings;');
                $stmt->execute();
                $dbVersion = $stmt->fetch(PDO::FETCH_ASSOC)['db_version'];
            } catch (PDOException $e) {
                print($e->getMessage());
                $dbVersion = 0;
            }
        }
        
        $files = scandir($migrationDir);
        foreach ($files as $file) {
            if (str_ends_with($file, '.sql')) {
                $scriptVersion = (int)explode('_', $file)[0];
                if ($scriptVersion > $dbVersion) {
                    $sqlScript = file_get_contents("$migrationDir/$file");
                    var_dump($sqlScript);
        
                    $database->exec($sqlScript);
                }
            }
        }
    }

    public static function isDatabaseInitialized( $databasePath ) {
        if (!file_exists($databasePath)) {
            return false;
        }

        $database = new PDO("sqlite:$databasePath");
        try {
            $stmt = $database->prepare('select count(id) as nb_user from user;');
            $stmt->execute();

            $nbUser = $stmt->fetch(PDO::FETCH_ASSOC)['nb_user'];
            return ($nbUser > 0);
        } catch (PDOException $e) {
            print($e->getMessage());
            return false;
        }
    }
}