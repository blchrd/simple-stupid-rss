<?php
class StupidRss
{
    public const LIMIT_PER_PAGE = 10;

    public static function initUser($databasePath, $username, $password)
    {
        $database = new PDO("sqlite:$databasePath");

        $username = $_POST['username'];
        $salt = base64_encode(random_bytes(32));
        $password = password_hash($salt . $_POST['password'], CRYPT_SHA256);

        $stmt = $database->prepare('insert into user (username, password, salt) values (:username, :password, :salt);');
        $stmt->bindParam('username', $username, PDO::PARAM_STR);
        $stmt->bindParam('password', $password, PDO::PARAM_STR);
        $stmt->bindParam('salt', $salt, PDO::PARAM_STR);

        $stmt->execute() or die("Error while creating the user");
    }

    public static function getItemFavCount($connection) {
        $stmt = $connection->prepare('select count(id) as nb_fav from item where favourite = 1;');
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC)['nb_fav'];
    }

    public static function getAllFeeds($connection)
    {
        $stmt = $connection->prepare('select * from feed order by title;');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function addFeed($title, $url, $connection)
    {
        $stmt = $connection->prepare('insert into feed (title, url) values (:title, :url);');
        $stmt->bindParam("title", $title, PDO::PARAM_STR);
        $stmt->bindParam("url", $url, PDO::PARAM_STR);

        $stmt->execute();
    }

    public static function deleteFeed($feedId, $connection)
    {
        $stmt = $connection->prepare('delete from item where feed_id = :id');
        $stmt->bindParam("id", $feedId, PDO::PARAM_INT);
        $stmt->execute();

        $stmt = $connection->prepare('delete from feed where id = :id');
        $stmt->bindParam("id", $feedId, PDO::PARAM_INT);
        $stmt->execute();
    }

    public static function getPaginatedItems($whereFilters, $page, $connection)
    {
        $numPage = is_numeric($page) || $page > 0 ? $page : 1;

        $query = "select item.*, feed.title as feed_title from item join feed on item.feed_id = feed.id";
        foreach ($whereFilters as $key => $cond) {
            $param = $cond['param'];
            if (!str_contains($query, ' where ')) {
                $query .= " where $key = $param";
            } else {
                $query .= " and $key = $param";
            }
        }
        $query .= " order by item.item_date desc;";

        $stmt = $connection->prepare($query);
        foreach ($whereFilters as $param) {
            $stmt->bindParam($param['param'], $param['value']);
        }
        $stmt->execute();

        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $startItem = ($numPage - 1) * Self::LIMIT_PER_PAGE;

        $lastPage = intdiv(count($items), Self::LIMIT_PER_PAGE) + 1;
        $items = array_slice($items, $startItem, self::LIMIT_PER_PAGE, true);
        
        return ['items' => $items, 'lastpage' => $lastPage];
    }

    public static function updateAllFeeds($connection)
    {
        $stmt = $connection->prepare("select * from feed");
        $stmt->execute();
        $feeds = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($feeds as $feed) {
            $timestamp = time();
            if ($feed["last_check"] + 3600 < $timestamp) {
                $atomFeed = false;
                try {
                    $rss = Feed::loadRss($feed['url']);
                    $items = $rss->item;
                } catch (FeedException $e) {
                    $rss = Feed::loadAtom($feed['url']);
                    $atomFeed = true;
                    $items = $rss->entry;
                }

                foreach ($items as $item) {
                    if ($item->timestamp > $feed["last_check"]) {
                        $stmt = $connection->prepare("insert into item (title, url, item_date, content, feed_id, read) values (:title, :url, :date, :content, :feedid, 0);");
                        $stmt->bindParam("title", $item->title, PDO::PARAM_STR);
                        $stmt->bindParam("url", $item->url, PDO::PARAM_STR);
                        $stmt->bindParam("date", $item->timestamp, PDO::PARAM_STR);
                        $stmt->bindParam("feedid", $feed["id"], PDO::PARAM_INT);

                        if (trim($item->content) != '') {
                            $stmt->bindValue("content", $atomFeed ? $item->content[0] : $item->{'content:encoded'}, PDO::PARAM_STR);
                        } else {
                            $stmt->bindValue("content", $item->description, PDO::PARAM_STR);
                        }


                        $stmt->execute();
                    }
                }

                $stmt = $connection->prepare("update feed set last_check = :date where id = :id");
                $stmt->bindParam("date", $timestamp, PDO::PARAM_STR);
                $stmt->bindParam("id", $feed["id"], PDO::PARAM_INT);
                $stmt->execute();
            }
        }
    }

    public static function updateFeedByFeedId($feedId, $connection)
    {

    }

    public static function cleanOldItem($connection)
    {
        $obsoleteTime = time() - (86400 * 5);
        $stmt = $connection->prepare("delete from item where read_at <= :time and read_at is not null and read = 1;");
        $stmt->bindParam("time", $obsoleteTime, PDO::PARAM_INT);

        $stmt->execute();
    }

    public static function favItem($itemId, $connection) {
        $stmt = $connection->prepare("update item set favourite = 1 where id = :id;");
        $stmt->bindParam('id', $itemId);
        $stmt->execute();
    }
    public static function unfavItem($itemId, $connection) {
        $stmt = $connection->prepare("update item set favourite = 0 where id = :id;");
        $stmt->bindParam('id', $itemId);
        $stmt->execute();
    }
    public static function markItemRead($itemId, $connection)
    {
        $timestamp = time();
        $stmt = $connection->prepare("update item set read = 1, read_at = :time where id = :id;");
        $stmt->bindParam('id', $itemId);
        $stmt->bindParam('time', $timestamp);
        $stmt->execute();
    }

    public static function markItemUnread($itemId, $connection)
    {
        $stmt = $connection->prepare("update item set read = 0, read_at = null where id = :id;");
        $stmt->bindParam('id', $itemId);
        $stmt->execute();
    }

    public static function markAllItemsRead($feedId, $connection)
    {
        $timestamp = time();
        $feed = isset($_GET['feed']) ? $_GET['feed'] : 0;
        if (!is_numeric($feed))
            $feed = 0;

        $query = 'update item set read = 1, read_at = :time' . ($feed != 0 ? ' where feed_id = :id;' : ';');
        $stmt = $connection->prepare($query);
        $stmt->bindParam('time', $timestamp);
        if ($feed != 0) {
            $stmt->bindParam('id', $feed, PDO::PARAM_INT);
        }

        $stmt->execute();
    }
}