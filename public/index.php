<?php
spl_autoload_register(function ($class) {
    require_once "../src/classes/$class.php";
});

$databasePath = '../database/feed.sqlite';
$migrationDir = '../database/migrations';
$databaseConnectionString = "sqlite:$databasePath";
$connection = null;

Session::init();

if (!Database::isDatabaseInitialized($databasePath)) {
    // installation process
    if (isset($_POST['username']) && isset($_POST['password'])) {
        Database::migrate($databasePath, $migrationDir);
        StupidRss::initUser($databasePath, $_POST['username'], $_POST['password']);
    } else {
        Template::view(
            '../src/templates/login.html',
            ['install' => true]
        );
        return;
    }
} else {
    Database::migrate($databasePath, $migrationDir);
}

try {
    $connection = new PDO($databaseConnectionString);
} catch (PDOException $e) {
    echo '' . $e->getMessage();
}

// Routing
if (isset($_GET["action"])) {
    switch ($_GET["action"]) {
        case 'login':
            if (isset($_POST['username']) && isset($_POST['password'])) {
                Session::login($_POST['username'], $_POST['password'], $connection);
            }

            header("Location: index.php");
            break;
        case 'logout':
            Session::logout();

            header('Location: index.php');
            break;
        case 'addfeed':
            if (isset($_POST["feedurl"]) && isset($_POST["feedtitle"])) {
                addFeed($_POST["feedtitle"], $_POST["feedurl"], $database);
            }
            break;
        case 'update':
            StupidRss::cleanOldItem($connection);
            StupidRss::updateAllFeeds($connection);
            return;
        case 'delete':
            if (isset($_GET["id"])) {
                StupidRss::deleteFeed($_GET['id'], $connection);
            }
            header('Location: index.php');
            break;
        case 'favitem':
            if (isset($_GET['id'])) {
                StupidRss::favItem($_GET['id'], $connection);
            }
            break;
        case 'unfavitem':
            if (isset($_GET['id'])) {
                StupidRss::unfavItem($_GET['id'], $connection);
            }
            break;
        case 'readitem':
            if (isset($_GET['id'])) {
                StupidRss::markItemRead($_GET['id'], $connection);
            }
            break;
        case 'unreaditem':
            if (isset($_GET['id'])) {
                StupidRss::markItemUnread($_GET['id'], $connection);
            }
            break;
        case 'markallread':
            StupidRss::markAllItemsRead($_GET['feed'], $connection);
            break;
        default:
            break;
    }
}

if (Session::isLogged()) {
    $view = '../src/templates/index.html';
    $args['feeds'] = StupidRss::getAllFeeds($connection);

    $read = isset($_GET['read']);
    $all = isset($_GET['all']);
    $feedId = isset($_GET['feed']) ? $_GET['feed'] : 0;
    $filters = array();

    if ($feedId === 'fav') {
        $filters['favourite'] = ['param' => ':fav', 'value' => 1];
    } else {
        if ($feedId != 0)
            $filters['feed_id'] = ['param' => ':feedid', 'value' => $feedId];
        if (!$all)
            $filters['read'] = ['param' => ':read', 'value' => ($read ? 1 : 0)];
    }


    $page = isset($_GET['page']) && (is_numeric($_GET['page']) || $_GET['page'] > 0) ? $_GET['page'] : 1;

    $datas = StupidRss::getPaginatedItems($filters, $page, $connection);

    if (count($datas['items']) == 0 && $page > 1) {
        header("Location: index.php?page=" . ($page - 1));
        return;
    }

    $args['items'] = $datas['items'];
    $args['nbfav'] = StupidRss::getItemFavCount($connection);
    $args['numpage'] = $page;
    $args['lastpage'] = $datas['lastpage'];
    $args['pagebasequery'] = '';
    $args['feedbasequery'] = '';
    $args['readbasequery'] = '';

    //build base query strings
    if (isset($_SERVER['QUERY_STRING'])) {
        $args['pagebasequery'] = join('&', array_filter(explode('&', $_SERVER['QUERY_STRING']), function ($param) {
            return !str_contains($param, 'page'); }));
        $args['feedbasequery'] = join('&', array_filter(explode('&', $_SERVER['QUERY_STRING']), function ($param) {
            return !str_contains($param, 'feed'); }));
        $args['readbasequery'] = join('&', array_filter(explode('&', $_SERVER['QUERY_STRING']), function ($param) {
            return str_contains($param, 'feed'); }));
    }
} else {
    $view = '../src/templates/login.html';
    $args = array();
}

Template::view(
    $view,
    $args,
);