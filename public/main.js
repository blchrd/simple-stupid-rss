var feedDetails = document.querySelectorAll(".column-item details")
feedDetails.forEach((element) => {
    element.addEventListener("toggle", function() {
        if (element.hasAttribute("open")) {
            markItemAsRead(element.id);
        }
    })
});

const favItem = (id) => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let element = document.getElementById(`fav-marker-${id}`);
            element.innerHTML = ` (fav) <button onclick="unfavItem(${id})">unfav</button>`;
            console.log(`Item id ${id} faved`);

            let nbFavElement = document.getElementById('nbfav');
            nbFavElement.innerHTML = (parseInt(nbFavElement.innerHTML) + 1);
        }
    }

    xhttp.open("GET", `index.php?action=favitem&item&id=${id}`);
    xhttp.send();
}

const unfavItem = (id) => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let element = document.getElementById(`fav-marker-${id}`);
            element.innerHTML = ` <button onclick="favItem(${id})">fav</button>`;
            console.log(`Item id ${id} faved`);

            let nbFavElement = document.getElementById('nbfav');
            nbFavElement.innerHTML = (parseInt(nbFavElement.innerHTML) - 1);
        }
    }

    xhttp.open("GET", `index.php?action=unfavitem&item&id=${id}`);
    xhttp.send();
}

const markItemAsRead = (id) => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let element = document.getElementById(`read-marker-${id}`);
            element.innerHTML = ` (read) <button onclick="markItemAsUnread(${id})">unread</button>`;
            console.log(`Item id ${id} read`);
        }
    }

    xhttp.open("GET", `index.php?action=readitem&id=${id}`);
    xhttp.send();
}

const markItemAsUnread = (id) => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let element = document.getElementById(`read-marker-${id}`);
            element.innerHTML = ` <button onclick="markItemAsRead(${id})">read</button>`;
            console.log(`Item id ${id} unread`);
        }
    }

    xhttp.open("GET", `index.php?action=unreaditem&id=${id}`);
    xhttp.send();
}

const updateFeeds = () => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            window.location.assign('index.php');
        }
    }

    xhttp.open("GET", 'index.php?action=update');
    xhttp.send();
}