create table user (id integer primary key autoincrement, username text, password text, salt text);
create table feed (id integer primary key autoincrement, url text, title text, last_check datetime);
create table item (id integer primary key autoincrement, feed_id int, url text, item_date datetime, title text, content text, read bool, read_at datetime, foreign key(feed_id) references feed(id));
create table settings (id integer primary key autoincrement, db_version int);
insert into settings (db_version) values (1);