<?php
spl_autoload_register(function ($class) {
    require_once 'src/classes/' . $class . '.php';
});
$databaseConnectionString = 'sqlite:database/feed.sqlite';
$database = null;
session_start();
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css" />
    <title>Simple&StupidRSS</title>
</head>
<script type="text/javascript" src="main.js" defer></script>

<body>
    <header>
        <h1><a href="index.php">Simple&Stupid RSS</a></h1>
        <?php
        $params = $_SERVER['QUERY_STRING'];
        $queryString = '';
        foreach (explode('&', $params) as $param) {
            if (str_contains($param, 'feed')) {
                $queryString .= $param . '&';
            }
        }
        if ($queryString == '&')
            $queryString = '';
        if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] = 1) {
            echo ' <a href="index.php?' . $queryString . '">Show unread</a>';
            echo ' <a href="index.php?' . $queryString . 'read">Show read</a>';
            echo ' <a href="index.php?' . $queryString . 'all">Show all</a>';
            echo ' <a href="index.php?' . $queryString . 'action=markallread">Mark all read</a>';
            echo ' <a href="index.php?logout">Logout</a>';
        }
        ?>
    </header>
    <main>
        <?php
        if (!file_exists('database/feed.sqlite')) {
            // We launch the install process
            install($databaseConnectionString);
            return;
        } else {
            // We check if we need to migrate to database
            $database = new PDO($databaseConnectionString);
        }

        if (!authenticate($database)) {
            return;
        } else if (isset($_GET["logout"])) {
            logout();
            return;
        }

        if (isset($_POST["feedurl"]) && isset($_POST["feedtitle"])) {
            addFeed($_POST["feedtitle"], $_POST["feedurl"], $database);
        }

        if (isset($_GET["action"])) {
            switch ($_GET["action"]) {
                case 'update':
                    updateFeeds($database);
                    return;
                case 'delete':
                    if (isset($_GET["id"])) {
                        deleteFeed($_GET['id'], $database);
                    }
                    break;
                case 'readitem':
                    if (isset($_GET['id'])) {
                        markItemRead($_GET['id'], $database);
                    }
                    break;
                case 'unreaditem':
                    if (isset($_GET['id'])) {
                        markItemUnread($_GET['id'], $database);
                    }
                    break;
                case 'markallread':
                    markAllRead($database);
                    break;
                default:
                    break;
            }
        }

        cleanOlderItem($database);
        displayFeed($database);

        function logout()
        {
            session_destroy();
            header("Location: index.php");
            return;
        }

        // Installation script
        function install($connectionString)
        {
            if (!file_exists('feed.sqlite')) {
                if (!isset($_POST['username'])) {
                    echo '<h2>Enter the credentials</h2>';
                    // Display the installation form
                    echo '<form action="index.php" method="post">
                <label for="username">Username</label><input name="username" id="username"/><br>
                <label for="password">Password</label><input type="password" id="password" name="password"/><br>
                <input type="submit"/>
            </form>';
                } else {
                    // Create database
                    $database = new PDO($connectionString);
                    $database->exec('create table user (id integer primary key autoincrement, username text, password text, salt text);');
                    $database->exec('create table feed (id integer primary key autoincrement, url text, title text, last_check datetime);');
                    $database->exec('create table item (id integer primary key autoincrement, feed_id int, url text, item_date datetime, title text, content text, read bool, read_at datetime, foreign key(feed_id) references feed(id));');

                    $username = $_POST['username'];
                    $salt = base64_encode(random_bytes(32));
                    $password = password_hash($salt . $_POST['password'], CRYPT_SHA256);

                    $stmt = $database->prepare('insert into user (username, password, salt) values (:username, :password, :salt);');
                    $stmt->bindParam('username', $username, PDO::PARAM_STR);
                    $stmt->bindParam('password', $password, PDO::PARAM_STR);
                    $stmt->bindParam('salt', $salt, PDO::PARAM_STR);

                    $stmt->execute() or die("Error while creating the user");

                    header("Location: index.php");
                }
            }
        }

        // Check if the user is authenticated
        function authenticate($db, $noForm = false)
        {
            if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] = 1) {
                return true;
            } else {
                if (isset($_POST["username"]) && isset($_POST["password"])) {
                    $username = $_POST["username"];
                    $password = $_POST["password"];

                    $stmt = $db->prepare("select * from user where username = :username");
                    $stmt->bindParam("username", $username, PDO::PARAM_STR);

                    $stmt->execute();
                    $user = $stmt->fetch(PDO::FETCH_ASSOC);

                    if (password_verify($user["salt"] . $password, $user["password"])) {
                        $_SESSION["loggedIn"] = 1;
                        $_SESSION["username"] = $username;

                        header("Location: index.php");
                        return true;
                    }
                } else if (!$noForm) {
                    echo '<form action="index.php" method="post">
                <label for="username">Username</label><input name="username" id="username"/><br>
                <label for="password">Password</label><input type="password" id="password" name="password"/><br>
                <input type="submit"/>
            </form>';
                }

                return false;
            }
        }

        function addFeed($title, $url, $db)
        {
            $stmt = $db->prepare('insert into feed (title, url) values (:title, :url);');
            $stmt->bindParam("title", $title, PDO::PARAM_STR);
            $stmt->bindParam("url", $url, PDO::PARAM_STR);

            $stmt->execute();
        }

        function deleteFeed($id, $db)
        {
            $stmt = $db->prepare('delete from item where feed_id = :id');
            $stmt->bindParam("id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $stmt = $db->prepare('delete from feed where id = :id');
            $stmt->bindParam("id", $id, PDO::PARAM_INT);
            $stmt->execute();

            header("Location: index.php");
        }

        // Update the feeds
        function updateFeeds($db)
        {
            $stmt = $db->prepare("select * from feed");
            $stmt->execute();
            $feeds = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($feeds as $feed) {
                $timestamp = time();
                if ($feed["last_check"] + 3600 < $timestamp) {
                    $atomFeed = false;
                    try {
                        $rss = Feed::loadRss($feed['url']);
                        $items = $rss->item;
                    } catch (FeedException $e) {
                        $rss = Feed::loadAtom($feed['url']);
                        $atomFeed = true;
                        $items = $rss->entry;
                    }

                    foreach ($items as $item) {
                        if ($item->timestamp > $feed["last_check"]) {
                            $stmt = $db->prepare("insert into item (title, url, item_date, content, feed_id, read) values (:title, :url, :date, :content, :feedid, 0);");
                            $stmt->bindParam("title", $item->title, PDO::PARAM_STR);
                            $stmt->bindParam("url", $item->url, PDO::PARAM_STR);
                            $stmt->bindParam("date", $item->timestamp, PDO::PARAM_STR);
                            $stmt->bindParam("feedid", $feed["id"], PDO::PARAM_INT);

                            if (trim($item->content) != '') {
                                $stmt->bindValue("content",$atomFeed ? $item->content[0] : $item->{'content:encoded'}, PDO::PARAM_STR);
                            } else {
                                $stmt->bindValue("content",$item->description, PDO::PARAM_STR);
                            }
                                 

                            $stmt->execute();
                        }
                    }

                    $stmt = $db->prepare("update feed set last_check = :date where id = :id");
                    $stmt->bindParam("date", $timestamp, PDO::PARAM_STR);
                    $stmt->bindParam("id", $feed["id"], PDO::PARAM_INT);
                    $stmt->execute();
                }
            }
        }

        // Delete older item (read + 5 days)
        function cleanOlderItem($db)
        {
            $obsoleteTime = time() - (86400 * 5);
            $stmt = $db->prepare("delete from item where read_at <= :time and read_at is not null and read = 1;");
            $stmt->bindParam("time", $obsoleteTime, PDO::PARAM_INT);

            $stmt->execute();
        }
        
        function markItemRead($id, $db)
        {
            $timestamp = time();
            $stmt = $db->prepare("update item set read = 1, read_at = :time where id = :id;");
            $stmt->bindParam('id', $id);
            $stmt->bindParam('time', $timestamp);
            $stmt->execute();
        }

        function markItemUnread($id, $db)
        {
            $stmt = $db->prepare("update item set read = 0, read_at = null where id = :id;");
            $stmt->bindParam('id', $id);
            $stmt->execute();
        }

        function markAllRead($db)
        {
            $timestamp = time();
            $feed = isset($_GET['feed']) ? $_GET['feed'] : 0;
            if (!is_numeric($feed))
                $feed = 0;

            $query = 'update item set read = 1, read_at = :time'.($feed != 0 ? ' where feed_id = :id;' : ';');
            $stmt = $db->prepare($query);
            $stmt->bindParam('time', $timestamp);
            if ($feed != 0) {
                $stmt->bindParam('id', $feed, PDO::PARAM_INT);
            }

            $stmt->execute();

            header("Location: index.php");
        }

        // Display the feeds
        function displayFeed($db)
        {
            $read = isset($_GET['read']);
            $all = isset($_GET['all']);
            $feedId = isset($_GET['feed']) ? $_GET['feed'] : 0;
            $where = array();

            if ($feedId != 0)
                $where["feed_id"] = ":feedid";
            if (!$all)
                $where["read"] = ":read";

            $query = "select item.*, feed.title as feed_title from item join feed on item.feed_id = feed.id";
            $queryCount = "select count(*) as nb_item from item";
            foreach ($where as $key => $cond) {
                if (!str_contains($query, ' where ')) {
                    $query .= " where $key = $cond";
                } else {
                    $query .= " and $key = $cond";
                }

                if (!str_contains($queryCount, ' where ')) {
                    $queryCount .= " where $key = $cond";
                } else {
                    $queryCount .= " and $key = $cond";
                }
            }
            $query .= " order by item.item_date desc";
            $queryCount .= ";";

            $page = isset($_GET["page"]) ? $_GET["page"] : 1;
            if (!is_numeric($page)) {
                header("Location: index.php");
                return;
            }
            if ($page <= 0)
                $page = 1;
            $startItem = ($page - 1) * 10;
            $query .= " limit $startItem, 10;";

            $stmt = $db->prepare($query);
            $stmtCount = $db->prepare($queryCount);
            if (!$all) {
                $stmt->bindValue("read", ($read ? 1 : 0), PDO::PARAM_INT);
                $stmtCount->bindValue("read", ($read ? 1 : 0), PDO::PARAM_INT);
            }
            if ($feedId != 0) {
                $stmt->bindParam("feedid", $feedId, PDO::PARAM_INT);
                $stmtCount->bindParam("feedid", $feedId, PDO::PARAM_INT);
            }

            $stmt->execute();
            $stmtCount->execute();

            $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $nbItem = $stmtCount->fetch()["nb_item"];
            $lastPage = intdiv($nbItem, 10) + 1;

            $stmt = $db->prepare("select * from feed order by title;");
            $stmt->execute();
            $feeds = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($items) == 0 && $page > 1) {
                header("Location: index.php?page=" . ($page - 1));
                return;
            }

            echo '<div class="row">';

            //List of feeds
            echo '<div class="column-feed">';
            echo '<ul>';
            $params = $_SERVER['QUERY_STRING'];
            $queryString = '';
            foreach (explode('&', $params) as $param) {
                if (!str_contains($param, 'feed')) {
                    $queryString .= $param . '&';
                }
            }
            if ($queryString == '&')
                $queryString = '';
            echo '<li><a href="index.php?' . $queryString . '">All feed</a><br>';
            foreach ($feeds as $feed) {
                echo '<li>';
                echo '<a href="index.php?' . $queryString . 'feed=' . $feed['id'] . '">' . htmlentities($feed["title"]) . '</a> ';
                echo '<a class="delete-link" href="index.php?action=delete&id=' . $feed['id'] . '">Delete</a>';
                echo '</li>';
            }
            echo '</ul>';
            echo '<div class="center"><button onclick="updateFeeds()">Update feed</button></div>';
            echo '<details><summary>Add feed</summary><form class="add-feed-form" action="index.php" method="post">
            <label for="feedurl">URL: </label><input name="feedurl" id="feedurl" /><br>
            <label for="feedtitle">Title: </label><input name="feedtitle" id="feedtitle" /><br>
            <input type="submit" value="Add feed" />
        </form></details>';
            echo '</div>';

            // List of items
            echo '<div class="column-item">';
            foreach ($items as $item) {
                $feed = $item["feed_title"];
                $id = $item["id"];
                $title = $item["title"];
                $date = is_numeric($item["item_date"]) ? date('Y-m-d H:i:s', $item["item_date"]) : "Date unknown";
                $content = $item["content"];
                $url = $item["url"];
                $read = '<span id="read-marker-' . $id . '">' . ($item["read"] == 1 ? ' (read) <button onclick="markItemAsUnread(' . $id . ')">unread</button>' : ' <button onclick="markItemAsRead(' . $id . ')">read</button>') . '</span>';
                echo "<details id=\"$id\"><summary>$date / $feed - $title$read</summary><div>$content</div><div class=\"center\"><a target=\"_blank\" href=\"$url\">Permalink</a></div></details><br>";
            }
            if (count($items) == 0) {
                echo '<h2 class="center">No items</h2>';
            } else {
                echo '<div class="center">';

                $params = $_SERVER['QUERY_STRING'];
                $queryString = '';
                foreach (explode('&', $params) as $param) {
                    if (!str_contains($param, 'page')) {
                        $queryString .= $param . '&';
                    }
                }
                if ($queryString == '&')
                    $queryString = '';

                if ($page > 1) {
                    echo '<a href="index.php?' . $queryString . '">&lt;&lt;</a> ';
                    echo '<a href="index.php?' . $queryString . 'page=' . ($page - 1) . '">&lt;</a> ';
                }
                echo $page;
                if ($page < $lastPage) {
                    echo ' <a href="index.php?' . $queryString . 'page=' . ($page + 1) . '">&gt;</a>';
                    echo ' <a href="index.php?' . $queryString . 'page=' . ($lastPage) . '">&gt;&gt;</a>';
                }
                echo '</div>';
            }
            echo '</div>';
            echo '</div>';
        }

        ?>
    </main>

    <footer>
        <hr width="500px">
        Copyleft blchrd - 2023<br>
        <a target="_blank" href="https://framagit.org/blchrd/simple-stupid-rss">Source code</a>
    </footer>
</body>

</html>