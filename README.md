# Simple&StupidRSS

A try to write a very simple RSS feed agregator, using SQLite database.

This is not designed to be used in production, it's just a fun project. It certainly lacks some very important security features for now. And it certainly lacks a lot of optimization and / or refactoring too.

The code is kind of a mess too. I'll eventually clean it up.

I added line 213 in Feed classes in order to test the project without having trouble with SSL certificate. **But I MUST remove that line before deploy this project anywhere.**

## Overview

![screenshot](screenshot.png)

## Getting started

You need to have PHP installed and pdo_sqlite / pdo_curl extension enabled.

```bash
git clone https://framagit.org/blchrd/simple-stupid-rss.git
cd simple-stupid-rss/public
php -S localhost:8080
```

Head to `localhost:8080` in your favourite browser, and here we go.

The installation screen will create the database and the user.

I keep the first "one-file" `index.php` for now, because reason.

**Please do not use this in a production environment.**

## Features

This section is also a TODO list for myself.

- [x] Init the database, and the min configuration
- [x] Update and display the feeds
  - [x] RSS
  - [x] Atom
- [x] Authentication
- [ ] CRUD for the feeds itself
  - [x] create
  - [x] read
  - [ ] update
  - [x] delete
- [x] Mark feed item as read / unread
- [x] Mark all item as read
- [x] Filters
  - [x] Read / unread
  - [x] Filter by feed
- [x] Pagination
- [x] Automatic cleanup of old items
- [x] Favourites
- [ ] Force feed update
- [ ] Categories
- [ ] Import / export OPML

## Database schema

### Table `user`

| Column Name | Data Type           |
| ----------- | ------------------- |
| `id`        | `int autoincrement` |
| `username`  | `text`              |
| `password`  | `text`              |
| `salt`      | `text`              |

### Table `feed`

| Column Name  | Data Type           |
| ------------ | ------------------- |
| `id`         | `int autoincrement` |
| `url`        | `text`              |
| `title`      | `text`              |
| `last_check` | `text`              |

### Table `item`

| Column Name | Data Type           |
| ----------- | ------------------- |
| `id`        | `int autoincrement` |
| `feed_id`   | `text`              |
| `url`       | `text`              |
| `date`      | `text`              |
| `title`     | `text`              |
| `content`   | `text`              |
| `read`      | `bool`              |
| `read_at`   | `text`              |
| `favourite` | `bool`              |

## Credits

Feed class I use has been written by dg it been used in accordance with the [licence](https://github.com/dg/rss-php/blob/master/license.md), the repo is available here https://github.com/dg/rss-php.

The Template class comes from [this blog post](https://codeshack.io/lightweight-template-engine-php/).

The session class was taken from [this blog post](https://tontof.net/?1334768400).
